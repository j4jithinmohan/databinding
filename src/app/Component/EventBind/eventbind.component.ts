import {Component} from '@angular/core';

@Component({
    selector: 'eventbind-info',
    templateUrl: './View/eventbind.html'
})

export class EventBindComponent{
    message:string = '';
    onClickMe(){
        this.message = 'Hi Good Morning';
    }
}