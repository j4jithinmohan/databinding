import {Component} from '@angular/core';

@Component({
    selector: 'propertybind-info',
    templateUrl: './View/propertybind.html'
})

export class PropertyBindComponent{
    data = {
        name: 'Maze Runner',        
        rating: 3,
        photo: './app/Assets/Images/maze_runner.jpg'
      };

      movie: any = this.data;
}