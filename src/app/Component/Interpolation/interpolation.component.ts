import {Component} from '@angular/core';

@Component({
    selector: 'interpolation-info',
    templateUrl: './View/interpolation.html'
})

export class InterpolationComponent{
    message:string = 'Good Morning';
}