import {Component} from '@angular/core';

@Component({
    selector: 'onewaybind-info',
    templateUrl: './View/onewaybind.html'
})

export class OneWayBindComponent{
    title:string = 'One way binding example';
    cityName:string = 'Kochi';
}