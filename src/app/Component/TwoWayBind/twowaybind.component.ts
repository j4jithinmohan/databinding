import {Component} from '@angular/core';

@Component({
    selector: 'twowaybind-info',
    templateUrl: './View/twowaybind.html'
})

export class TwoWayBindComponent{
    title:string = 'Two way binding example';
    myName:string = 'Jithin';
}