import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';

import {TwoWayBindComponent} from '../../Component/TwoWayBind/twowaybind.component';

@NgModule({
    imports: [BrowserModule, FormsModule],
    declarations: [TwoWayBindComponent],
    bootstrap: [TwoWayBindComponent]
})

export class TwoWayBindModule{}