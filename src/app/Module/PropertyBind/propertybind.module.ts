import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {PropertyBindComponent} from '../../Component/PropertyBind/propertybind.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [PropertyBindComponent],
    bootstrap: [PropertyBindComponent]
})

export class PropertyBindModule{}