import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {InterpolationComponent} from '../../Component/Interpolation/interpolation.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [InterpolationComponent],
    bootstrap: [InterpolationComponent]
})

export class InterpolationModule{}