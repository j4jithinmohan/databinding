import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {OneWayBindComponent} from '../../Component/OneWayBind/onewaybind.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [OneWayBindComponent],
    bootstrap: [OneWayBindComponent]
})

export class OneWayBindModule{}