import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {EventBindComponent} from '../../Component/EventBind/eventbind.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [EventBindComponent],
    bootstrap: [EventBindComponent]
})

export class EventBindModule{}