import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';

import { AppComponent }  from './app.component';
import {InterpolationComponent} from './Component/Interpolation/interpolation.component';
import {OneWayBindComponent} from './Component/OneWayBind/onewaybind.component';
import {TwoWayBindComponent} from './Component/TwoWayBind/twowaybind.component';
import {EventBindComponent} from './Component/EventBind/eventbind.component';
import {PropertyBindComponent} from './Component/PropertyBind/propertybind.component';

@NgModule({
  imports:      [ BrowserModule,FormsModule ],
  declarations: [ AppComponent,InterpolationComponent,OneWayBindComponent,TwoWayBindComponent,
                  EventBindComponent,PropertyBindComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
